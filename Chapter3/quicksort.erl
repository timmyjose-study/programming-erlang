-module(quicksort).
-export([quicksort/1]).

quicksort([]) -> [];

quicksort([X]) -> [X];

quicksort([H | T]) ->
  Smaller = [X || X <- T, X =< H],
  Bigger = [Y || Y <- T, Y > H],
  quicksort(Smaller) ++ [H] ++ quicksort(Bigger).

