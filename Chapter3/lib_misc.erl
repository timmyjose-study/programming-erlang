-module(lib_misc).
-export([sum/1,
         for/3,
         pythag/1,
         perms/1,
         max/2,
         myfilter/2,
         myabs/1,
         odds_and_evens/1,
         odds_and_evens_acc/1,
         qsort/1]).

sum(L) -> sum(L, 0).

sum([], Acc) -> Acc;
sum([H | T], Acc) -> sum(T, Acc + H).

for(Max, Max, F) -> [F(Max)];
for(I, Max, F) -> [F(I) | for(I + 1, Max, F)].

qsort([]) -> [];

qsort([X | []]) -> [X];

qsort([H | T]) ->
  Smaller = [X || X <- T, X =< H],
  Bigger = [Y || Y <- T, Y > H],
  qsort(Smaller) ++ [H] ++ qsort(Bigger).

pythag(N) ->
  [{A, B, C} || 
   A <- lists:seq(1, N),
   B <- lists:seq(A, N),
   C <- lists:seq(A, N),
   A + B + C =< N,
   A * A + B * B =:= C * C].

% permutations of a list.

perms([]) -> [[]];
perms(L) -> [[H | T] || H <- L, T <- perms(L -- [H])].

max(X, Y) when X >= Y -> X;
max(_X, Y) -> Y.

myfilter(_P, []) -> [];

myfilter(P, [H | T]) ->
  case P(H) of
    true -> [H | myfilter(P, T)];
    false -> myfilter(P, T)
  end.

myabs(N) ->
  if 
    N < 0 -> -N;
    true -> N
  end.

odds_and_evens(L) ->
  Odds = [O || O <- L, O rem 2 =:= 1],
  Evens = [E || E <- L, E rem 2 =:= 0],
  {Odds, Evens}.

odds_and_evens_acc(L) -> odds_and_evens_acc(L, [], []).

odds_and_evens_acc([], Odds, Evens) -> {lists:reverse(Odds), lists:reverse(Evens)};

odds_and_evens_acc([H | T], Odds, Evens) ->
  case H rem 2 of
    0 -> odds_and_evens_acc(T, Odds, [H | Evens]);
    1 -> odds_and_evens_acc(T, [H | Odds], Evens)
  end.
