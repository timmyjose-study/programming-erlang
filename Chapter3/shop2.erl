-module(shop2).
-export([total/1]).

cost(oranges) -> 5;
cost(newspaper) -> 8;
cost(apples) -> 2;
cost(pears) -> 9;
cost(milk) -> 7.

total(Items) -> 
  mylists:sum(mylists:map(fun ({What, N}) -> cost(What) * N end, Items)).

