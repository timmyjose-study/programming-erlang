-module(lib_misc).
-export([sqrt/1]).

sqrt(X) when X < 0 ->
  erlang:error({square_root_negative_argument, X});

sqrt(X) ->
  math:sqrt(X).
