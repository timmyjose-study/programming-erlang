-module(try_test).
-export([demo1/0, 
         demo2/0]).

generate_exception(1) -> a;
generate_exception(2) -> throw(a);
generate_exception(3) -> exit(a);
generate_exception(4) -> {'EXIT', a};
generate_exception(5) -> erlang:error(a).

demo1() ->
  [catcher(I) || I <- lists:seq(1, 5)].

catcher(N) ->
  try generate_exception(N) of
    Val -> {N, normal, Val}
  catch
    throw:T -> {N, caught, thrown, T};
    exit:E -> {N, caught, exited, E};
    error:Err -> {N, caught, error, Err}
  end.

demo2() ->
  [{I, catch generate_exception(I)} || I <- lists:seq(1, 5)].

% demo3() ->
%   try generate_exception(5)
%   catch
%     error:E ->
%       {E, erlang:get_stacktrace()} % deprecated
%   end.